package com.preskale.de.slackalerts;

import org.springframework.stereotype.Service;

@Service
public interface SlackService {
	void reportProblem(String problem);
}
