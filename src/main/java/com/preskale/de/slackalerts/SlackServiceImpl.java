package com.preskale.de.slackalerts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.hubspot.slack.client.SlackClient;
import com.hubspot.slack.client.SlackClientFactory;
import com.hubspot.slack.client.SlackClientRuntimeConfig;
import com.hubspot.slack.client.methods.params.chat.ChatPostMessageParams;

@Service
public class SlackServiceImpl implements SlackService {
	SlackClientRuntimeConfig runtimeConfig = SlackClientRuntimeConfig.builder()
            .setTokenSupplier(() -> "xoxb-2065203500960-2401866126370-2WbAbA7ZwITnNVBe7FthBasu")
            .build();

     SlackClient slackClient = SlackClientFactory.defaultFactory().build(runtimeConfig);
     private final String channel = "data-engineering-monitoring";
 	Logger LOGGER = LoggerFactory.getLogger(SlackServiceImpl.class);

	@Override
	public void reportProblem(String problem) {
		// TODO Auto-generated method stub
		LOGGER.debug("Sending message to channel {}: {}", channel, problem);
        slackClient.postMessage(
            ChatPostMessageParams.builder()
                .setText(problem)
                .setChannelId(channel)
                .build()
        ).join().unwrapOrElseThrow();
		
	}

}
