package com.preskale.de.commons;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.util.StringUtils;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;

@Configuration
public class BaseMongoConnectionService extends AbstractMongoClientConfiguration {

	@Value("${spring.data.mongodb.database}")
	private String databaseName;

	@Value("${spring.data.mongodb.uri}")
	private String uri;

	private MongoClient client = null;

	private final HashMap<String, MongoTemplate> dbObjectMap = new HashMap<>();

	@Override
	public MongoClient mongoClient() {
		if (client == null) {

			final ConnectionString connectionString = new ConnectionString(uri);
			final MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
					.applyConnectionString(connectionString).build();
			client = MongoClients.create(mongoClientSettings);
		}
		return client;
	}

	@Override
	protected String getDatabaseName() {
		return databaseName;
	}

	protected MongoDatabase getDatabase(String database) {
		if (StringUtils.isEmpty(database)) {
			database = getDatabaseName();
		}
		return mongoClient().getDatabase(database);
	}

	protected MongoTemplate getMongoTemplate(final String database) {
		if (StringUtils.isEmpty(database)) {
			return getMongoTemplate();
		}
		if (dbObjectMap.containsKey(database)) {
			return dbObjectMap.get(database);
		} else {
			final MongoTemplate template = new MongoTemplate(mongoClient(), database);
			dbObjectMap.put(database, template);
			return template;
		}
	}

	@Bean
	protected MongoTemplate getMongoTemplate() {
		return new MongoTemplate(mongoClient(), getDatabaseName());
	}

}
