package com.preskale.de.commons;

import org.bson.Document;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperationContext;

public class CustomAggregationOperation implements AggregationOperation {
	private final Document operation;

	public CustomAggregationOperation(final Document operation) {
		this.operation = operation;
	}

	@Override
	public Document toDocument(final AggregationOperationContext context) {
		return context.getMappedObject(operation);
	}
}