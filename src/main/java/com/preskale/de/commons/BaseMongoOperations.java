package com.preskale.de.commons;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoIterable;

@Repository
public class BaseMongoOperations {

	@Autowired
	private BaseMongoConnectionService service;

	protected MongoCollection<Document> getCollection(final String Collection) {
		return service.getDatabase(null).getCollection(Collection);
	}

	protected MongoCollection<Document> getCollection(final String database, final String Collection) {
		return service.getDatabase(database).getCollection(Collection);
	}

	protected <T> List<T> mongoCursorIteratorReturn(final MongoIterable<Document> iterator,
			final IPreskaleMongoCursorIteratorReturn<T> iteratorReturn) {
		final List<T> result = new ArrayList<>();
		try (MongoCursor<Document> cursor = iterator.cursor()) {
			while (cursor.hasNext()) {
				result.add(iteratorReturn.call(cursor.next()));
			}
		}
		return result;
	}

	protected void mongoCursorIterator(final MongoIterable<Document> iterator,
			final IPreskaleMongoCursorIterator iteratorImpl) {
		try (MongoCursor<Document> cursor = iterator.cursor()) {
			while (cursor.hasNext()) {
				iteratorImpl.call(cursor.next());
			}
		}
	}

	@FunctionalInterface
	protected interface IPreskaleMongoCursorIteratorReturn<T> {
		T call(Document document);
	}

	@FunctionalInterface
	protected interface IPreskaleMongoCursorIterator {
		void call(Document document);
	}

	protected MongoTemplate getMongoTemplate() {
		return service.getMongoTemplate(null);
	}

	protected MongoTemplate getMongoTemplate(final String database) {
		return service.getMongoTemplate(database);
	}

}
