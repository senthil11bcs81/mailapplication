package com.preskale.de.mail;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.mail.search.FlagTerm;
import com.google.common.collect.Lists;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCursor;
import com.preskale.de.commons.BaseMongoOperations;
import java.util.*;

import javax.mail.*;

@Service
public class EmailServiceImpl extends BaseMongoOperations implements EmailService {
	Logger LOGGER = LoggerFactory.getLogger(EmailServiceImpl.class);

	@Override
	public String registerImap(String user_id, String server, String port, String username, String password)
			throws Exception {
		// TODO Auto-generated method stub
		Session session = Session.getDefaultInstance(new Properties());
		Store store = session.getStore("imaps");
		store.connect(server, Integer.parseInt(port), username, password);
		if (store.isConnected()) {
			System.out.println("Store is Connected");
			BasicDBObject obj = new BasicDBObject();
			obj.put("user_id", user_id);
			obj.put("server", server);
			obj.put("port", port);
			obj.put("username", username);
			obj.put("password", password);
			store.close();
			return addInboxToDB(obj, EmailEnum.USER_INBOX.getValue(), EmailEnum.USERNAME.getValue(),
					EmailEnum.DATABASE.getValue());
		} else {
			throw new Exception("Invalid Credentials. Authentication Failed");
		}

	}

	@SuppressWarnings("unchecked")
	public String addInboxToDB(final BasicDBObject doc, final String collectionName, final String type,
			final String database) {
		try {
			final Document taskFromDB = getCollection(database, collectionName)
					.find(new BasicDBObject(type, doc.get(type).toString())).first();
			if (taskFromDB == null) {
				LOGGER.info("Adding Inbox " + type + ":" + doc.get(type));
				getMongoTemplate(database).insert(doc, collectionName);
				ObjectId id = (ObjectId) doc.get("_id");
				return id.toHexString();
			} else {
				// LOGGER.info("Updating Inbox " + type + ":" + doc.get(type));
				// final Query query1 = new
				// Query(Criteria.where(type).is(doc.get(type).toString()));
				// final Update update = new Update();
				// getMongoTemplate(database).updateFirst(query1, update,
				// collectionName).getModifiedCount();
				return ("Already Added inbox_id = " + taskFromDB.getObjectId("_id").toHexString());
			}
		} catch (final Exception e) {
			e.printStackTrace();
			return ("Exception occured");
		}
	}

	@SuppressWarnings("unchecked")
	public String addUserToDB(final BasicDBObject doc, final String collectionName, final String type,
			final String database) {
		try {
			final Document taskFromDB = getCollection(database, collectionName)
					.find(new BasicDBObject(type, doc.get(type).toString())).first();
			if (taskFromDB == null) {
				LOGGER.info("Adding user " + type + ":" + doc.get(type));
				getMongoTemplate(database).insert(doc, collectionName);
				ObjectId id = (ObjectId) doc.get("_id");
				return id.toHexString();
			} else {
				// LOGGER.info("Updating user " + type + ":" + doc.get(type));
				// final Query query1 = new
				// Query(Criteria.where(type).is(doc.get(type).toString()));
				// final Update update = new Update();
				// BsonValue id = getMongoTemplate(database).updateFirst(query1, update,
				// collectionName).getUpsertedId();
				// if(id == null)
				return ("Already Added user_id = " + taskFromDB.getObjectId("_id").toHexString());
				// return id.toString().toString();
			}
		} catch (final Exception e) {
			e.printStackTrace();
			return ("Exception occured");
		}
	}

	@Override
	public String registerUser(String username) throws Exception {
		// TODO Auto-generated method stub
		if (username != null && username.length() > 0) {
			BasicDBObject obj = new BasicDBObject();
			obj.put("user", username);
			return addUserToDB(obj, EmailEnum.USER_INFO.getValue(), EmailEnum.USER.getValue(),
					EmailEnum.DATABASE.getValue());
		} else {
			throw new Exception("Invalid Request");
		}
	}

	@Override
	public List<Document> userlist(String user_id) throws Exception {
		// TODO Auto-generated method stub
		MongoCursor<Document> iterator = getCollection(EmailEnum.DATABASE.getValue(), EmailEnum.USER_INBOX.getValue())
				.find().iterator();
		List<Document> inbox = Lists.newArrayList();
		while (iterator.hasNext()) {
			inbox.add(iterator.next());
		}
		LOGGER.info("inbox size = " + inbox.size());
		return inbox;
	}

	@Override
	public Message inboxDetails(String inbox_id) throws Exception {
		Document doc = getCollection(EmailEnum.DATABASE.getValue(), EmailEnum.USER_INBOX.getValue())
				.find(new BasicDBObject(EmailEnum.INBOX_ID.getValue(), new ObjectId(inbox_id))).first();
		System.out.println(doc);
		Message msg = new Message();
		List<String> list = Lists.newArrayList();
		if (doc != null) {
			Session session = Session.getDefaultInstance(new Properties());
			Store store = session.getStore("imaps");
			store.connect(doc.get("server").toString(), Integer.parseInt(doc.get("port").toString()),
					doc.get("username").toString(), doc.get("password").toString());
			if (store.isConnected()) {
				LOGGER.info("Store is Connected");
				Folder inbox = store.getFolder("INBOX");
				inbox.open(Folder.READ_ONLY);
				// total messages
				msg.setTotalMessages(inbox.getMessageCount());
				msg.setNewMessages(inbox.getNewMessageCount());
				msg.setUnread_count(inbox.getUnreadMessageCount());
				msg.setDeletedMessages(inbox.getDeletedMessageCount());
				// Fetch unseen messages from inbox folder
				javax.mail.Message[] messages = inbox.search(new FlagTerm(new Flags(Flags.Flag.RECENT), false));
				msg.setRecent_count(messages.length);
				for (javax.mail.Message message : messages) {
					list.add(message.getSubject());
				}
				msg.setRecent_messages(list);
				System.out.println(messages.length);
				inbox.close();
				store.close();
				return msg;
			} else {
				LOGGER.info("Store is Not Connected");
			}
		}
		throw new Exception("Authentication Failed");
	}

}
