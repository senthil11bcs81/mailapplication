package com.preskale.de.mail;

import java.util.List;

import org.bson.Document;

public interface EmailService {

	String registerImap(String user_id, String server, String port, String username, String password) throws Exception;

	String registerUser(String username) throws Exception;

	List<Document> userlist(String user_id) throws Exception;

	Message inboxDetails(String inbox_id) throws Exception;
}
