package com.preskale.de.mail;

public enum EmailEnum {
	DATABASE("senthil"), USER_INFO("user_info"), USER_INBOX("user_inbox"), USER("user"), INBOX_ID("_id"),
	USERID("user_id"), USERNAME("username");

	private String value;

	EmailEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
}
