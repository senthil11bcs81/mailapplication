package com.preskale.de.mail;

import java.util.List;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(path = "/mail")
public class MailController {

	@Autowired
	EmailServiceImpl emailService;

	Logger LOGGER = LoggerFactory.getLogger(MailController.class);

	@GetMapping("/imap/register")
	public ResponseEntity<String> imapsetup(@RequestParam("user_id") String user_id,
			@RequestParam("server") String server, @RequestParam("port") String port,
			@RequestParam("username") String username, @RequestParam("password") String password) {
		LOGGER.info(user_id + " : " + server + " : " + port + " : " + username + " : " + password);
		try {
			return new ResponseEntity<String>(emailService.registerImap(user_id, server, port, username, password),
					HttpStatus.OK);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return new ResponseEntity<String>(e.getMessage().toString(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/imap/list")
	public ResponseEntity<List<Document>> imaplist(@RequestParam("user_id") String user_id) {
		LOGGER.info(user_id);
		List<Document> list = Lists.newArrayList();
		try {
			list = emailService.userlist(user_id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return new ResponseEntity<List<Document>>(list, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<Document>>(list, HttpStatus.OK);
	}

	@GetMapping("/imap/inbox/details")
	public ResponseEntity<Message> imapinboxdetails(@RequestParam("inbox_id") String inbox_id) {
		LOGGER.info(inbox_id);
		Message response = null;
		try {
			response = emailService.inboxDetails(inbox_id);
			System.out.println(response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return new ResponseEntity<Message>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Message>(response, HttpStatus.OK);
	}

	@GetMapping("/imap/usersetup")
	public ResponseEntity<String> usersetup(@RequestParam("user") String user) {
		LOGGER.info(user);
		try {
			return new ResponseEntity<String>(emailService.registerUser(user), HttpStatus.OK);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return new ResponseEntity<String>(e.getMessage().toString(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
