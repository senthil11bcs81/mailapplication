package com.preskale.de.mail;

import java.util.List;

import lombok.Data;

@Data
public class Message {
	int totalMessages;
	int recent_count;
	int newMessages;
	int deletedMessages;
	int unread_count;
    List<String> recent_messages;
}
