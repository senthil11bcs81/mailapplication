# Spring boot

MailAPI Spring Boot application with REST API.

# Run

To build and run from a packaged jar locally:

    gradle build
    ./gradlew run
    
# API'S

    curl http://localhost:8080/mail/imap/usersetup?user=senthil


    curl http://localhost:8080/mail/imap/register?user_id=6129ad3a426559557c7d9d51&server=outlook.office365.com&port=993&username=vinterviewcandidate@outlook.com&password=Welcome@123

    curl http://localhost:8080/mail/imap/list?user_id=6129ad3a426559557c7d9d51


    curl http://localhost:8080/mail/imap/inbox/details?inbox_id=6129afcd7154f72045a65e9e
